package pl.edu.pwr.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.edu.pwr.pp.ImageFileReader;
import pl.edu.pwr.utils.ApplicationUtils;

import java.awt.image.BufferedImage;
import java.io.File;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

/**
 * Created by Kamil on 2016-06-01.
 */
public class ApplicationUtilsTest {

    private BufferedImage image;


    @Before
    public void setUp() {
        image = new BufferedImage(200, 200, BufferedImage.TYPE_BYTE_GRAY);
    }

    @Test
    public void shouldReturnConvertedImageInGray() {
        BufferedImage image2 = ApplicationUtils.resizeImage(image, BufferedImage.TYPE_BYTE_GRAY, 100, 100);

        Assert.assertThat(image2.getWidth(), is(equalTo(100)));
        Assert.assertThat(image2.getHeight(), is(equalTo(100)));
    }

    @Test
    public void shouldReadFileFromDisk() {
        String path = null;
        try {
            path = new File(".").getCanonicalPath() +"\\target\\test-classes\\random-image-15.jpg";

        } catch (Exception e) {
            Assert.fail();
        }
        BufferedImage image2 = ApplicationUtils.getFromDisc(path);
        Assert.assertNotNull(image2);
    }

    @Test
    public void shouldGetFromWeb(){
        String url = "http://science-all.com/images/wallpapers/random-image/random-image-15.jpg";
        BufferedImage image3 = ApplicationUtils.getFromWeb(url);
        Assert.assertNotNull(image3);
    }


}
