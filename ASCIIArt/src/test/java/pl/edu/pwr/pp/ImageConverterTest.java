package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.edu.pwr.utils.ApplicationUtils;

import javax.imageio.ImageReader;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.internal.verification.VerificationModeFactory.atLeastOnce;

/**
 * Created by Kamil on 2016-06-07.
 */
public class ImageConverterTest {

    private BufferedImage testImage;

    @Mock
    private BufferedImage image;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        try {
            testImage = ApplicationUtils.getFromDisc(new File(".").getCanonicalPath() + "\\target\\test-classes\\random-image-15.jpg");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldConvertPgmImage() {
        char[][] convertedImage = ImageConverter.convertImage(testImage, ImageConverter.GreyLevels.HIGHT);
        Assert.assertThat(convertedImage.length, is(equalTo(testImage.getHeight())));
        Assert.assertThat(convertedImage[0].length, is(equalTo(testImage.getWidth())));
    }

    @Test
    public void shouldConvertUsingSize() {
        Mockito.when(image.getWidth()).thenReturn(10);
        Mockito.when(image.getHeight()).thenReturn(20);

        char[][] result = ImageConverter.convertImage(image, ImageConverter.GreyLevels.LOW);

        Mockito.verify(image, atLeastOnce()).getWidth();
        Mockito.verify(image, atLeastOnce()).getHeight();
        Mockito.verify(image, atLeastOnce()).getType();
    }
}
