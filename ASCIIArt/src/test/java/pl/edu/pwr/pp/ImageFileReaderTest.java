package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.edu.pwr.pp.ImageFileReader;

import java.io.File;
import java.nio.file.NoSuchFileException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ImageFileReaderTest {

    ImageFileReader imageReader;

    @Before
    public void setUp() {
        imageReader = new ImageFileReader();
    }

    @Test
    public void shouldReadSequenceFrom0To255GivenTestImage() {
        // given
        String fileName = "testImage.pgm";
        // when
        int[][] intensities = null;
        try {
            fileName = new File(".").getCanonicalPath() + "\\target\\test-classes\\testImage.pgm";
            intensities = imageReader.readPgmFile(fileName);
        } catch (Exception e) {
            Assert.fail("Should read the file");
        }
        // then
        int counter = 0;
        for (int[] row : intensities) {
            for (int intensity : row) {
                assertThat(intensity, is(equalTo(counter++)));
            }
        }
    }

    @Test
    public void shouldThrowExceptionWhenFileDontExist() {
        // given
        String fileName = "nonexistent.pgm";
        imageReader = new ImageFileReader();
        try {
            // when
            imageReader.readPgmFile(fileName);
            // then
            Assert.fail("Should throw exception");
        } catch (Exception e) {
            assertThat(e, is((instanceOf(NoSuchFileException.class))));
        }

    }


}
