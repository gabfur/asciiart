package pl.edu.pwr.utils;

import pl.edu.pwr.pp.ImageConverter;
import pl.edu.pwr.pp.ImageFileReader;
import pl.edu.pwr.pp.ImageFileWriter;
import pl.edu.pwr.view.ReadImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Kamil on 2016-05-24.
 */
public class ApplicationUtils {


    public static char[][] converPGM(int[][] intensities) {
        char[][] ascii = ImageConverter.intensitiesToAscii(intensities, ImageConverter.GreyLevels.LOW);
        return ascii;
    }

    public static void saveToFile(char[][] ascii, String path) {
        ImageFileWriter.saveToTxtFile(ascii, path);
    }

    public static BufferedImage getFromDisc(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public static BufferedImage getFromWeb(String path) {
        BufferedImage image = null;
        try {
            URL url = new URL(path);
            image = ImageIO.read(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public static BufferedImage resizeImage(BufferedImage originalImage, int type, int w, int h) {
        BufferedImage resizedImage = new BufferedImage(w, h, type);
        Graphics2D g = resizedImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(originalImage, 0, 0, w, h, null);
        g.dispose();
        return resizedImage;
    }
}
