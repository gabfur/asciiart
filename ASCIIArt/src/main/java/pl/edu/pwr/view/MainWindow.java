package pl.edu.pwr.view;

import pl.edu.pwr.pp.ImageConverter;
import pl.edu.pwr.utils.ApplicationUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;


/**
 * Created by Kamil on 2016-05-24.
 */
public class MainWindow extends JFrame {
    private static final int INIT_WIDTG = 600;
    private static final int INIT_HEIGHT = 400;
    private JButton wczytajObrazButton;
    private JPanel rootPanel;
    private JPanel imagePannel;
    private JLabel picLabel;
    private JButton saveToFile;
    private JButton funkcja1;
    private JButton funkcja2;
    private JComboBox opcja1;
    private JComboBox opcja2;
    private JButton ClearButton;


    ImageIcon imgIcon;
    BufferedImage image;
    char[][] ascii;

    public MainWindow() {
        super("AsciArt");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.getContentPane().add(rootPanel);
        //this.getContentPane().add(imagePanel);
        createUIComponents();
        pack();
        setVisible(true);
    }

    private void createUIComponents() {
        opcja1.setModel(new DefaultComboBoxModel<>((ImageConverter.GreyLevels.values())));
        opcja2.setModel(new DefaultComboBoxModel<>(new String[]{"Orginal", "80", "160", "Screen size"}));

        saveToFile.setEnabled(false);
        wczytajObrazButton.addActionListener(
                e -> {
                    if (e.getSource() == wczytajObrazButton) {
                        ReadImage reader = new ReadImage();
                        image = reader.getOption();
                        if (image != null) {
                            BufferedImage resizedImage = ApplicationUtils.resizeImage(image, image.getType(), rootPanel.getWidth() - wczytajObrazButton.getWidth(), rootPanel.getHeight() - funkcja2.getHeight());
                            imgIcon = new ImageIcon(resizedImage);
                            picLabel.setIcon(imgIcon);
                            saveToFile.setEnabled(true);
                        }
                        picLabel.setVisible(true);
                        rootPanel.updateUI();
                    }
                });

        rootPanel.addComponentListener(
                new ComponentAdapter() {
                    @Override
                    public void componentResized(ComponentEvent e) {
                        if (image != null) {
                            BufferedImage resizedImage = ApplicationUtils.resizeImage(image, image.getType(), rootPanel.getWidth() - wczytajObrazButton.getWidth(), rootPanel.getHeight() - funkcja2.getHeight());
                            imgIcon = new ImageIcon(resizedImage);
                            picLabel.setIcon(imgIcon);
                            picLabel.setVisible(true);
                            rootPanel.updateUI();
                        }
                    }
                }
        );
        ClearButton.addActionListener(
                e -> {
                    picLabel.setDisabledIcon(imgIcon);
                    picLabel.setVisible(false);
                });
        saveToFile.addActionListener(
                e -> {
                    BufferedImage resizedImage = null;
                    double s1 = image.getHeight();
                    double s2 = image.getWidth();
                    double stosunek = s1 / s2;
                    switch ((String) opcja2.getSelectedItem()) {
                        case "Orginal":
                            resizedImage = image;
                            break;
                        case "80":
                            resizedImage = ApplicationUtils.resizeImage(image, image.getType(), 80, (int) (80 * stosunek));
                            break;
                        case "160":
                            resizedImage = ApplicationUtils.resizeImage(image, image.getType(), 160, (int) (160 * stosunek));
                            break;
                        case "Screen size":
                            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                            resizedImage = ApplicationUtils.resizeImage(image, image.getType(), (int) screenSize.getWidth(), (int) (screenSize.getWidth() * stosunek));
                            break;
                    }
                    if (resizedImage != null) {
                        ascii = ImageConverter.convertImage(resizedImage, (ImageConverter.GreyLevels) opcja1.getSelectedItem());
                        SaveImage writer = new SaveImage(ascii);

                        //char[][] ascii=ApplicationUtils.converPGM(intensity);
                        String path = writer.getPath();
                        picLabel.setText("The file path: " + path);
                        picLabel.setVisible(true);
                        rootPanel.updateUI();
                        //ApplicationUtils.saveToFile(ascii,path);
                    }
                }

        );
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(INIT_WIDTG, INIT_HEIGHT);
    }
}
