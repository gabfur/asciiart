package pl.edu.pwr.view;

import pl.edu.pwr.utils.ApplicationUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;


/**
 * Created by Gabi on 2016-05-29.
 */
public class SaveImage extends JDialog {

    private static final int INIT_WIDTG = 400;
    private static final int INIT_HEIGHT = 500;
    private JButton getPahtButton;
    private JLabel fileUriLabel;
    private JButton confirmButton;
    private JPanel SavePanel;
    private JButton cancelButton;
    String Path;
    char[][] ascii;

    SaveImage(char[][] asci) {
        super();
        ascii = asci;
        this.setContentPane(SavePanel);
        createUIComponents();
        setModal(true);
        pack();
        setVisible(true);
    }

    private void createUIComponents() {
        fileUriLabel.setText("No path selected");
        boolean state = true;
        getPahtButton.setEnabled(state);

        getPahtButton.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            //FileNameExtensionFilter filter = new FileNameExtensionFilter("txt");
            //  chooser.addChoosableFileFilter(filter);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.showOpenDialog(new JFrame());
            File f = chooser.getSelectedFile();
            if (f != null) {
                fileUriLabel.setText(f.getAbsolutePath());
                Path = f.getAbsolutePath();
            }
        });


        cancelButton.addActionListener(
                e -> {
                    CloseFrame();
                });
        confirmButton.addActionListener(
                e -> {
                    if (Path.toString().toLowerCase().endsWith(".txt"))
                        ApplicationUtils.saveToFile(ascii, Path);
                    setModal(false);
                    setVisible(false);
                });
    }

    public String getPath() {
        return Path;
    }

    public void CloseFrame() {
        super.dispose();
    }

    public Dimension getPreferredSize() {
        return new Dimension(INIT_WIDTG, INIT_HEIGHT);
    }
}