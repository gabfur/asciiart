package pl.edu.pwr.view;

import com.sun.jndi.toolkit.url.Uri;
import pl.edu.pwr.pp.ImageFileReader;
import pl.edu.pwr.utils.ApplicationUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.JOptionPane;
import java.util.stream.IntStream;

/**
 * Created by Kamil on 2016-05-24.
 * <p/>
 * Okno odpowiedzialne za umożliwienie pobrania obrazka.
 */
public class ReadImage extends JDialog {
    private BufferedImage image;
    private char[][] ascii;
    private String format;
    private JRadioButton fromDiscRadio;
    private JPanel fileURIPanel;
    private JRadioButton fromUrlRadio;
    private JButton getPahtButton;
    private JLabel fileUriLabel;
    private JTextField fileUrlTextField;
    private JButton confirmButton;
    private JButton cancelButton;


    ReadImage() {
        super();
        this.setContentPane(fileURIPanel);
        createUIComponents();
        setModal(true);
        pack();
        setVisible(true);


    }

    private void createUIComponents() {
        fileUriLabel.setText("No path selected");
        fileUrlTextField.setEnabled(false);
        getPahtButton.setEnabled(false);

        fromDiscRadio.addActionListener(
                e -> {
                    boolean state = !fromDiscRadio.isSelected();
                    fileUriLabel.setEnabled(!state);
                    getPahtButton.setEnabled(!state);
                    fromUrlRadio.setSelected(state);
                    fileUrlTextField.setEnabled(state);

                    if (state) {
                        fileUrlTextField.setText("No path selected");

                    }
                });
        fromUrlRadio.addActionListener(
                e -> {
                    boolean state = !fromUrlRadio.isSelected();
                    fileUrlTextField.setEnabled(!state);
                    getPahtButton.setEnabled(state);
                    fromDiscRadio.setSelected(state);
                    if (state) {
                        fileUriLabel.setText("No path selected");
                    }
                });
        getPahtButton.addActionListener(
                e -> {
                    JFileChooser chooser = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "jpg", "png", "gif", "bmp", "pgm");//bez pgm???
                    chooser.addChoosableFileFilter(filter);
                    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    chooser.showOpenDialog(new JFrame());
                    File f = chooser.getSelectedFile();
                    if (f != null) {
                        fileUriLabel.setText(f.getAbsolutePath());
                    }
                });
        cancelButton.addActionListener(
                e -> {
                    fileUriLabel.setText("No path selected");
                    CloseFrame();
                });

        confirmButton.addActionListener(
                e -> {        // najepiej, gdby był to jakiś obiekt typu Image, jak nie niech będzie ścierzka do niego...
                    if (fromDiscRadio.isSelected()) {
                        String sciezka = fileUriLabel.getText();
                        // String ext = FilenameUtils.getExtension("/path/to/file/foo.txt");

                        FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "jpg", "png", "gif", "bmp", "pgm");
                        if (sciezka.toString().toLowerCase().endsWith(".pgm")) {
                            format = "pgm";
                            ImageFileReader imageFileReader = new ImageFileReader();
                            try {
                                int[][] intensity = imageFileReader.readPgmFile(sciezka);

                                image = new BufferedImage(intensity[0].length, intensity.length, BufferedImage.TYPE_BYTE_GRAY);
                                WritableRaster raster = image.getRaster();

                                for (int y = 0; y < intensity.length; y++)
                                    for (int x = 0; (x < intensity[0].length); x++)
                                       // IntStream.range(0, intensity[0].length).forEach(
                                             //   x -> raster.setSample(x, y, 0, intensity[y][x]));
                                        raster.setSample(x, y, 0, intensity[y][x]);

//                                    ascii = ApplicationUtils.converPGM(intensity);
                            } catch (Exception exa) {
                                exa.printStackTrace();
                            }

                        } else if (sciezka.toString().toLowerCase().endsWith(".txt")) {

                            JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                            JOptionPane.showMessageDialog(frame, "Bad file extension");

                        } else {

                            format = "inny";
                            image = ApplicationUtils.getFromDisc(sciezka);
                        }


                    }
                    if (fromUrlRadio.isSelected()) {
                        image = ApplicationUtils.getFromWeb(fileUrlTextField.getText());
                    }


                        //TODO: tego nie ruszaj
                        setModal(false);
                        setVisible(false);

                });
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 300);
    }

    //TODO: Jak zmienisz typ option zmień typ zwracany przez metodę
    public BufferedImage getOption() {
        return image;
    }

    public char[][] getAscii() {
        return ascii;
    }

    public boolean formatPGM() {
        if (format == "pgm") {
            return true;
        } else {
            return false;
        }
    }

    public void CloseFrame() {
        super.dispose();
    }
}
