package pl.edu.pwr.pp;

import java.io.*;
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.nio.file.Path;
//import java.nio.file.Paths;

public class ImageFileWriter {

    public static void saveToTxtFile(char[][] ascii, String fileName) {
        // np. korzystając z java.io.PrintWriter
        // TODO Wasz kod

        try(PrintWriter writer = new PrintWriter(fileName, "UTF-8")) {
            for (char[] row : ascii) {
                for (char c : row)
                    writer.write(c);
                writer.write("\n");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
