package pl.edu.pwr.pp;

import java.awt.*;
import java.awt.image.*;
import java.util.stream.IntStream;
public class ImageConverter {

    /**
     * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
     * do białego (255).
     */
    public enum GreyLevels {
        LOW, HIGHT
    }

    ;
    private static String INTENSITY_2_ASCII = "@%#*+=-:. ";
    private static String INTENSITY_2_ASCII_EXTRA = "\"$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. \"";

    public static char intensityToAscii(int intensity) {
        int przedzial;
        przedzial = (int) ((intensity * 100) / (25.6 * 100));
        return INTENSITY_2_ASCII.charAt(przedzial);
    }

    public static char intensityToAsciiWithLevel(int intensity, GreyLevels level) {
        int przedzial;
        char znak;
        if (level == GreyLevels.LOW) {
            przedzial = (int) ((intensity * 100) / (25.6 * 100));
            znak = INTENSITY_2_ASCII.charAt(przedzial);
        } else {
            przedzial = (int) ((intensity * 100) / (3.62 * 100));
            znak = INTENSITY_2_ASCII_EXTRA.charAt(przedzial);
        }
        return znak;

    }

    public static char[][] intensitiesToAscii(int[][] intensities, GreyLevels level) {
        // TODO Wasz kod
        int rows = intensities.length;
        int columns = intensities[0].length;
        char[][] tablicaZnakowASCII = new char[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                char znak = intensityToAsciiWithLevel(intensities[i][j], level);
                tablicaZnakowASCII[i][j] = znak;
            }
        }

        return tablicaZnakowASCII;
    }

    public static char[][] convertImage(BufferedImage image, GreyLevels level) {
        int[][] intensities = new int[image.getHeight()][image.getWidth()];
        if (image.getType() == BufferedImage.TYPE_BYTE_GRAY) {
            BufferedImage gray = new BufferedImage(image.getWidth(), image.getHeight(),
                    BufferedImage.TYPE_BYTE_GRAY);
            BufferedImageOp grayscaleConv =  new ColorConvertOp(
                    image.getColorModel().getColorSpace(),
                    gray.getColorModel().getColorSpace(), null);
            grayscaleConv.filter(image, gray);
        }
      /* for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
             Color color = new Color(image.getRGB(j, i));
              intensities[i][j] = (int) (0.2989 * color.getRed() + 0.5870 * color.getGreen() + 0.1140 * color.getBlue());
            }
            }*/


        IntStream.range(0, image.getHeight()).forEach( i-> {
            IntStream.range(0, image.getWidth()).forEach(  j -> {
                Color color = new Color(image.getRGB(j, i));
                intensities[i][j] = (int) (0.2989 * color.getRed() + 0.5870 * color.getGreen() + 0.1140 * color.getBlue());

            });
        });

        return ImageConverter.intensitiesToAscii(intensities, level);
    }


}
