package pl.edu.pwr.pp;

import com.sun.jndi.toolkit.url.Uri;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageFileReader {

	/**
	 * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
	 *
	 *
	 *            nazwa pliku pgm
	 * @return tablica odcieni szarości odczytanych z pliku
	 * @throws URISyntaxException
	 *             jeżeli plik nie istnieje
	 */


	//public int[][] readPgmFile(String fileName) throws URISyntaxException {
		public int[][] readPgmFile(String sciezka) throws URISyntaxException, IOException {
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;
        	//File img=image.readImage();
			Path path = this.getPathToFile(sciezka);

//            FileReader fr=new FileReader(sciezka);
			BufferedReader reader =  Files.newBufferedReader(path);
			String p2 = reader.readLine();
			//System.out.println(p2);
			if (p2.charAt(0) != 'P' || p2.charAt(1) != '2') {
				System.out.println("To nie jest plik PGM!!!");
			}
					// TODO : Wasz kod
			String line = reader.readLine();
			//System.out.println("to jest linia druga" + line);
			if (line.charAt(0) != '#') {
				System.out.println("Cos nie tak z tym plikeim nie ma#!!");
			}

			line = reader.readLine();
			String[] rozmiary = line.split(" ");
			columns = Integer.parseInt(rozmiary[0]);
			rows = Integer.parseInt(rozmiary[1]);
			line = reader.readLine();
			int maxintensity = Integer.parseInt(line);
			if (maxintensity != 255) {
				System.out.println("Hej, ten obraz ma inna maxymalna szarosc!");
			}
			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[rows][];
			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					// System.out.println(intensities[currentRow][currentColumn]);
					currentColumn++;
					if (currentColumn == columns) {
						currentRow++;
						currentColumn = 0;
					}
				}
			}

		return intensities;
	}

	private Path getPathToFile(String fileName) throws  URISyntaxException {
	//	URI uri = ClassLoader.getSystemResource(fileName).toURI();
		return Paths.get(fileName);
	}

}
